#include "Database.h"
#include <functional>
#include <QFile>

namespace LibQPamac {
Database::Database(PamacDatabase *db, QObject *parent):
    QObject(parent)
{
}

Database::Database(const QString& configFile, QObject *parent):
    QObject(parent)
    {
    m_config = new Config(configFile,this);
    handle = pamac_database_new(m_config->handle());
    init();
}

//QFile Database::cloneBuildFiles(const QString &pkgname, bool overwrite) const{

//    auto gfile = pamac_database_clone_build_files(handle,pkgname.toUtf8(),gboolean(overwrite),nullptr);

//    return QFile(g_file_get_path(gfile));
//}

QStringList Database::getRepos()
{
    return Utils::glibToQtArray<char*, QString>(pamac_database_get_repos_names(handle));
}

QStringList Database::getGroups()
{
    return Utils::glibToQtArray<char*, QString>(pamac_database_get_groups_names(handle));
}

void Database::getCategoryPackagesAsync(const QString &category, std::function<void (QList<AlpmPackage>)> callback)
{
    pamac_database_get_category_pkgs_async(handle,category.toUtf8(), [](GObject* self, GAsyncResult* res, gpointer cb){
            
            auto result = pamac_database_get_category_pkgs_finish(reinterpret_cast<PamacDatabase*>(self), res);
            
            auto callback = reinterpret_cast<std::function<void (QList<AlpmPackage>)>*>(cb);
            
            (*callback)(Utils::glibToQtArray<PamacAlpmPackage*, AlpmPackage>(result));
            
            delete callback;
            
        }, std::move(&callback));
}

QList<AlpmPackage> Database::searchPkgs(const QString &name)
{
    return Utils::glibToQtArray<PamacAlpmPackage*, AlpmPackage>(pamac_database_search_pkgs(handle,name.toUtf8()));
}

QList<AlpmPackage> Database::getGroupPackages(const QString &group)
{
    return Utils::glibToQtArray<PamacAlpmPackage*, AlpmPackage>(pamac_database_get_group_pkgs(handle,group.toUtf8()));
}

QList<AlpmPackage> Database::getRepoPackages(const QString &repo)
{
    return Utils::glibToQtArray<PamacAlpmPackage*, AlpmPackage>(pamac_database_get_repo_pkgs(handle,repo.toUtf8()));

}

void Database::getInstalledAppsAsync(std::function<void (QList<AlpmPackage>)> callback){
    pamac_database_get_installed_apps_async(handle, [](GObject* self, GAsyncResult* res, gpointer cb){
            
            auto result = pamac_database_get_installed_apps_finish(reinterpret_cast<PamacDatabase*>(self), res);
            
            auto callback = reinterpret_cast<std::function<void (QList<AlpmPackage>)>*>(cb);
            
            (*callback)(Utils::glibToQtArray<PamacAlpmPackage*, AlpmPackage>(result));
            
            delete callback;
            
        }, std::move(&callback));
}

//QStringList Database::getMirrorsCountries(){
//    QStringList result = Utils::gsListToQStringList(pamac_database_get_mirrors_countries(handle),true);
//    result.push_front("Worldwide");
//    return result;
//}

//QString Database::getMirrorsChoosenCountry(){
//    gchar* res = pamac_database_get_mirrors_choosen_country(handle);
//    QString result = QString::fromUtf8(res);
//    g_free(res);
//    return result;

//}

Updates Database::getUpdates(){
    return {pamac_database_get_updates(handle,false)};
}

AlpmPackage Database::getInstalledPackage(const QString &name){
    return {pamac_database_get_installed_pkg(handle,name.toUtf8())};
}

AlpmPackage Database::getSyncPackage(const QString &name){
    return {pamac_database_get_sync_pkg(handle,name.toUtf8())};
}

AurPackage Database::getAurPackage(const QString &name){
    return {pamac_database_get_aur_pkg(handle,name.toUtf8())};
}

QList<AlpmPackage> Database::findPackagesByName(const QStringList &names)
{
    QList<AlpmPackage> packages;
    for(auto& name: names){
        AlpmPackage pkg;
        if((pkg = getInstalledPackage(name)).name().isEmpty()){
            if((pkg = getSyncPackage(name)).name().isEmpty()){

            }
        }
        packages.append(pkg);

    }
    return packages;
}

QStringList Database::getHistory(){
    QFile file("/var/log/pacman.log");
    file.open(QFile::ReadOnly);
    QStringList list;
    while(!file.atEnd()){
        QString line = QString::fromUtf8(file.readLine());
        if(line.contains("[ALPM]")){
            list.append(line);
        }
    }
    file.close();

    return list;
}

void Database::setConfig(Config *config)
{
    m_config = config;
    Q_EMIT configChanged(config);
}


void Database::init()
{
    pamac_database_enable_appstream(handle);

    g_signal_connect(handle,"get_updates_progress",
                     reinterpret_cast<GCallback>(+[](GObject* obj,uint percent,Database* t){
                         Q_UNUSED(obj);
                         Q_EMIT t->getUpdatesProgress(percent);
                     }),this);
}

QList<AlpmPackage> LibQPamac::Database::getInstalledPackages(Database::InstalledPackageTypes type){
    GPtrArray* result;
    switch (type) {
    case Installed:
        result = pamac_database_get_installed_pkgs(handle);
        break;
    case Explicitly:
        result = pamac_database_get_explicitly_installed_pkgs(handle);
        break;
    case Orphans:
        result = pamac_database_get_orphans(handle);
        break;
    case Foreign:
        result = pamac_database_get_foreign_pkgs(handle);
        break;
    }
    
    return LibQPamac::Utils::glibToQtArray<PamacAlpmPackage*, AlpmPackage>(result);
}



QList<AurPackage> Database::getAurPackages(const QStringList &nameList)
{

//    auto list = new std::vector<char*>(Utils::qStringListToCStringVector(nameList));

//    auto resultGlist = pamac_database_get_aur_pkgs(handle,list->data(),list->size());
//    QList<AurPackage> result;
//    for(auto el = g_hash_table_get_values(resultGlist);el!=nullptr;el=el->next){
//        result.append(QVariant::fromValue(AurPackage::fromData(el->data)));
//    }
//    return result;
}

AlpmPackage Database::getPkg(const QString &name)
{
   return {pamac_database_get_pkg(handle,name.toUtf8())};
}

} // namespace LibQPamac
