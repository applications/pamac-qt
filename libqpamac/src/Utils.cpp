#include <glib.h>
#include "Utils.h"
#include <cstring>

GVariant *LibQPamac::Utils::qVariantToGVariant(const QVariant& value)
{
    if (!value.isValid())
        return nullptr;

    switch (value.type()) {
    case QVariant::Invalid:
        return nullptr;
    case QVariant::Bool:
        return g_variant_new_boolean(value.toBool());
    case QVariant::Int:
        return g_variant_new_int32(value.toInt());
    case QVariant::UInt:
        return g_variant_new_uint32(value.toUInt());
    case QVariant::LongLong:
        return g_variant_new_int64(value.toLongLong());
    case QVariant::ULongLong:
        return g_variant_new_uint64(value.toULongLong());
    case QVariant::Double:
        return g_variant_new_double(value.toDouble());
    case QVariant::Char:
        return g_variant_new_byte(value.toChar().toLatin1());
    case QVariant::String:
        return g_variant_new_string(value.toString().toUtf8());
    case QVariant::ByteArray:
        return g_variant_new_bytestring(value.toByteArray());
    default:
        return nullptr;
    }
}

