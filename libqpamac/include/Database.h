#pragma once
#include <QObject>
#include <Package.h>
#include <memory>
#include <Config.h>
#include <Updates.h>
#include <HistoryItemModel.h>
#include <AlpmPackage.h>
#include <AurPackage.h>
#include <pamac.h>

namespace LibQPamac {
class Database:public QObject
{
    Q_OBJECT
    Q_PROPERTY(Config* config READ config WRITE setConfig NOTIFY configChanged)

public:

    enum InstalledPackageTypes{
        Installed,
        Explicitly,
        Orphans,
        Foreign
    };
    Q_ENUM(InstalledPackageTypes)

    enum PackageTypes{
        Repos,
        AUR
    };
    Q_ENUM(PackageTypes)

    Database(PamacDatabase* m_db,QObject* parent = nullptr);
    Database(const QString &configFile, QObject* parent = nullptr);

//     QFile cloneBuildFiles(const QString& pkgname,bool overwrite = true) const;


     QStringList getRepos();
     QStringList getGroups();
     
     QList<AurPackage> searchPkgsInAur(const QString &name);
     
     void getCategoryPackagesAsync(const QString &category, std::function<void(QList<AlpmPackage> packages)> callback);
     QList<AlpmPackage> searchPkgs(const QString &name);
     QList<AlpmPackage> getGroupPackages(const QString &group);
     QList<AlpmPackage> getRepoPackages(const QString &repo);
     
     void getInstalledAppsAsync(std::function<void (QList<AlpmPackage>)> callback);
     
     QList<AlpmPackage> getInstalledPackages(InstalledPackageTypes type);
     QList<AurPackage> getAurPackages(const QStringList &nameList);
     AlpmPackage getPkg(const QString& name);

    Config* config()
    {
        return m_config;
    }
    PamacDatabase* getHandle(){
        return handle;
    }

//     QStringList getMirrorsCountries();
//     QString getMirrorsChoosenCountry();

     Updates getUpdates();

     AlpmPackage getInstalledPackage(const QString& name);

     AlpmPackage getSyncPackage(const QString& name);
     AurPackage getAurPackage(const QString& name);

     QList<AlpmPackage> findPackagesByName(const QStringList& names);


    inline void refresh(){
        pamac_database_refresh(handle);
    }

     QStringList getHistory();
     
     
     
 public Q_SLOTS:
    void setConfig(Config *config);

Q_SIGNALS:
    void updatesReady(Updates upds);
    void getUpdatesProgress(uint percent);



    void configChanged(Config* config);


private:
    PamacDatabase* handle;
    void init();
    Config* m_config = nullptr;
    bool m_asynchronous;
};

} //namespace LibQPamac
