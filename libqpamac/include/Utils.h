#pragma once
#include "pamac.h"
#include <vector>
#include <functional>
#include <type_traits>
#include <QVariantList>
#include <QStringList>
#include <QDateTime>
#include <QHash>
#include <QtDebug>

namespace LibQPamac::Utils{

#define PAMAC_QT_PROPERTY_GET(type, name, method) \
    Q_PROPERTY(type name READ name CONSTANT)\
    inline type name() const \
{\
    return type(method);\
}

#define PAMAC_QT_URL_PROPERTY_GET(name,method)\
    PAMAC_QT_PROPERTY_GET(QUrl, name, method)

#define PAMAC_QT_INT_PROPERTY_GET(name,method)\
    PAMAC_QT_PROPERTY_GET(int, name, method)

#define PAMAC_QT_DOUBLE_PROPERTY_GET(name,method)\
    PAMAC_QT_PROPERTY_GET(double, name, method)


#define PAMAC_QT_STRING_PROPERTY_GET(name,method)\
    Q_PROPERTY(QString name READ name CONSTANT)\
    inline QString name() const \
{\
    return QString::fromUtf8(method);\
}

#define PAMAC_QT_DATETIME_PROPERTY_GET(name,method)\
    Q_PROPERTY(QDateTime name READ name CONSTANT)\
    inline QDateTime name()\
{\
    return QDateTime::fromSecsSinceEpoch(g_date_time_to_unix(method));\
}

#define PAMAC_QT_STRINGLIST_PROPERTY_GET(name,method)\
    Q_PROPERTY(QStringList name READ name CONSTANT)\
    inline QStringList name() const\
{\
    auto tmp = method;\
QStringList result = LibQPamac::Utils::glibToQtArray<char*, QString>(tmp);\
    return result;\
}

#define PAMAC_QT_STRING_PROPERTY_GET_SET(getName,getMethod,setName,setMethod)\
    Q_PROPERTY(QString getName READ getName WRITE setName)\
    inline QString getName()\
{\
    return QString::fromUtf8(getMethod);\
}\
    inline void setName(const QString& getName)\
{\
    setMethod;\
}




#define PAMAC_QT_PROPERTY_GET_SET(type, getName, getMethod, setName, setMethod) \
    Q_PROPERTY(type getName READ getName WRITE setName NOTIFY on ## getName ## Changed)\
Q_SIGNALS:\
    void on ## getName ## Changed();\
public:\
    inline type getName() const\
{\
    return type(getMethod);\
}\
    inline void setName(type getName){\
    setMethod;\
    on ## getName ## Changed();\
}

#define PAMAC_QT_INT_PROPERTY_GET_SET(getName,getMethod,setName,setMethod)\
    PAMAC_QT_PROPERTY_GET_SET(int,getName,getMethod,setName,setMethod)

#define PAMAC_QT_UINT_PROPERTY_GET_SET(getName,getMethod,setName,setMethod)\
    PAMAC_QT_PROPERTY_GET_SET(uint,getName,getMethod,setName,setMethod)

#define PAMAC_QT_BOOL_PROPERTY_GET_SET(getName,getMethod,setName,setMethod)\
    PAMAC_QT_PROPERTY_GET_SET(bool,getName,getMethod,setName,setMethod)



GVariant* qVariantToGVariant(const QVariant&);

template<typename TGlibObject, typename TQtObject>
TQtObject glibToQt(TGlibObject obj);

template<>
inline QDateTime glibToQt(GDateTime* datetime) {
    return QDateTime::fromSecsSinceEpoch(g_date_time_to_unix(datetime));
}

template<>
inline QString glibToQt(char* value){
    return QString::fromUtf8(value);
}



template<typename TGlibObject, typename TQtObject>
QList<TQtObject> glibToQtArray(GPtrArray* array){
    QList<TQtObject> result;
    g_ptr_array_foreach(array, [](gpointer arrayValue, gpointer userData){
            QList<TQtObject>* result = reinterpret_cast<QList<TQtObject>*>(userData);
            
            result->append(glibToQt<TGlibObject, TQtObject>(
                reinterpret_cast<TGlibObject>(arrayValue)));
        }, &result);
}


template<typename TK, typename TV>
QHash<TK,TV> glibToQtHash(const GHashTable* array){
    
}


static QMap<gpointer*, void*> wrapperAccessMap; //to access wrapper class from overridden methods

} // namespace LibQPamac::Utils
