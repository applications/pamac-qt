#pragma once
#include "pamac.h"
#include <QObject>
#include "Utils.h"
#include "Package.h"
#include "AurPackage.h"
#include "AlpmPackage.h"
namespace LibQPamac {
class Updates
{
    Q_GADGET
public:
    Updates()=default;
    Updates(PamacUpdates* upd):m_updates(std::shared_ptr<PamacUpdates>(upd,g_object_unref)){}
    inline QList<AlpmPackage> getReposUpdates(){
        return Utils::glibToQtArray<PamacAlpmPackage*, AlpmPackage>(pamac_updates_get_repos_updates(m_updates.get()));
    }
    inline QList<AurPackage> getAurUpdates(){
        return Utils::glibToQtArray<PamacAURPackage*, AurPackage>(pamac_updates_get_aur_updates(m_updates.get()));
    }
private:
    std::shared_ptr<PamacUpdates> m_updates;
};
} //namespace LibQPamac
